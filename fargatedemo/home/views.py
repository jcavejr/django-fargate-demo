from django.shortcuts import render
import socket

# Create your views here.
def home(request):
    visits = 1
    if request.session.get("visits"):
        visits = request.session.get("visits") + 1
    request.session["visits"] = visits
    context = {
        "hostname": socket.gethostname(),
        "visits": visits
    }
    return render(request, 'index.html', context)
