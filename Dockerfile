FROM python:3.9

# Install dependencies
COPY requirements.txt /tmp/requirements.txt
RUN apt update && \
    apt install -y rustc && \
    pip install -r /tmp/requirements.txt && \
    rm -rf /tmp

# Copy the app
COPY fargatedemo /app/

WORKDIR /app

EXPOSE 80

CMD ["daphne", "fargatedemo.asgi:application", "-b", "0.0.0.0", "-p", "80"]
