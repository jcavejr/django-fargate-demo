# Fargate Django Demo
This repository serves as a demo of django on AWS Fargate

## Development setup
Install rust,  create a virtual environment, and install the python requirements:
```zsh
brew install rustup
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

## Running the app locally
To run in debug mode:
```zsh
cd fargatedemo
python manage.py runserver
```
The app is then viewable at localhost:8000

To run in a single Docker container:
```zsh
docker-compose build
docker run --rm -p 8000:8000 django-demo:0.1
```
The app is then viewable at localhost:8000

To run the app with Docker compose with load balancing:
```zsh
docker-compose build
docker-compose up --scale site=n
```
Where `n` is the number of apps you want to run. The app is then viewable at localhost:4000.

## Implementation notes
One of the concerns I had about running django in Fargate is how django handles sessions. Sessions, by default, are stored in a sqlite database stored on the server. Since the Fargate containers will come up and down when necessary, we cannot rely on the default django session stores. Because of this, the home page intentionally stores a "visit" count in the user's session.

If you run the app as a single Docker container, this is fine. If you instead run the app as multiple Docker containers with a load balancer, each of the different containers have their own session stores so the visit count will be inaccurate.